Eksamiks vajalikud failid asuvad https://bitbucket.org/mkalmo/icd0019exam0.
Töö tegemiseks kloongige see projekt oma arvutisse.

Kopeerige see tekst kloonitud projektis olevasse faili info.txt.

Töö esitamiseks peate käivitama programmi, mis teie tehtud muudatusi
automaatselt üles laeb. Selle kohta on info failis monitor.txt.

Aega on 90 minutit. Arvesse läheb seis, mis on esitatud enne kella 17:30.

Teooriaküsimused

  Iga küsimuse vastus peab jääma alla 140 märgi.
  Vastus peab olema kirjutatud eesti keeles.
  Iga vastus annab kuni 5 punkti.
  Oluline on vastata täpselt sellele, mida küsitakse.

  1. Millal kasutada võrdlemiseks equals() meetodit ja millal == operaatorit?

     ... vastus tuleb siia ...

  2. Mis kasu on koodi kompileerimisest?


  3. NullPointerException-it catch plokis ei püüta. Miks?

  4. ...

  5. ...

Ülesanne 1 (10 punkti)

  Kataloogis ex1 on klass MyQueue ja test selle testimiseks.

  MyQueue on klass, mis kujutab järjekorda. Lisame elemente järjekorra lõppu ja
  võtame elemente järjekorra lõpust või algusest.

  Kirjutage see klass lõpuni, et testid klassis MyQueueTest tööle hakkaksid.
  Dünaamilisi kollektsioone, ei ole lubatud kasutada.

  Ei pea arvestama olukorraga, kus add() meetodit kutsutakse välja rohkem kui 1000 korda.

Ülesanne 2 (15 punkti)

  ...

